<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\User::truncate();
        App\User::create([
                        'account' => 'admin',
                        'password'=>\Hash::make('12345678'),
                        'email'=>'',
                        'name'=>'admin',
                        'tel'=>'',
                        'birthday'=>'',
                    ]);
    }
}
