<?php
use App\Product;

function getProductByProductCategory($id, $paginate = false)
{
	if($paginate){
		$product = Product::where('product_category_id', $id)->paginate($paginate);
	} else {
    	$product = Product::where('product_category_id', $id)->get();
    }

    return $product;

}