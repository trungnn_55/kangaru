<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'products';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['product_category_id', 'name', 'detail', 'path_img', 'show_detail', 'sign', 'feature', 'technical'];

    public static $ruleAdd = [
                            'name' => 'required|unique:products,name',
                            'sign' => 'required|unique:products,sign',
                            'path_img' => 'required'
                            ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

    public static function getAllProduct()
    {
        return Product::all();
    }

    public static function getProductByCategory($id)
    {
        $products = Product::where('product_category_id', $id) ->get(); //->paginate(PER_PAGE);

        return $products;
    }

    public static function addProduct($input, $category_id)
    {
        $product['name'] = $input['name'];
        $product['sign'] = $input['sign'];
        $product['product_category_id'] = $category_id;
        $product['detail'] = $input['detail'];
        $product['show_detail'] = $input['show_detail'];
        $product['feature'] = $input['feature'];
        $product['technical'] = $input['technical'];

        $file = \Input::file('path_img');
        if(isset($file)) {
            $name = $file->getClientOriginalName();
            $file->move(public_path().'/images/product', $name);
            $product['path_img'] = '/images/product/' . $input['path_img']->getClientOriginalName();
        }
        // dd($product);

        Product::create($product);
    }

    public static function editProduct($input, $id)
    {
        $product['name'] = $input['name'];
        $product['sign'] = $input['sign'];
        $product['detail'] = $input['detail'];
        $product['show_detail'] = $input['show_detail'];
        $product['feature'] = $input['feature'];
        $product['technical'] = $input['technical'];
        $file = \Input::file('path_img');
        if(isset($file)) {
            $name = $file->getClientOriginalName();
            $file->move(public_path().'/images/product', $name);
            $product['path_img'] = '/images/product/' . $input['path_img']->getClientOriginalName();
        }
        // dd($product);

        Product::find($id)->update($product);
    }
}
