<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'news';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'detail', 'news_category_id', 'viewer', 'path_img'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

    public static function getAllNews()
    {
        return News::orderBy('created_at', 'desc')->get();
    }

    public static function getNewsByCategory($id)
    {
        $news = News::where('news_category_id', $id)->paginate(PER_PAGE);

        return $news;
    }

    public static function addNews($input, $id)
    {
        $news['name'] = $input['name'];
        $news['detail'] = $input['detail'];
        $news['path_img'] = '/images/news/' . $input['path_img']->getClientOriginalName();

        News::create($news);
    }

    public static function editNews($input, $id)
    {
        $news['name'] = $input['name'];
        $news['detail'] = $input['detail'];
        $news['path_img'] = '/images/news/' . $input['path_img']->getClientOriginalName();

        News::find($id)->update($news);
    }

    public static function listNews($id)
    {
        $categoryNews = News::find($id)->news_category_id;
        $listNews = News::where('news_category_id', $categoryNews)->orderBy('created_at', 'desc')->take(5)->get();

        return $listNews;
    }
}
