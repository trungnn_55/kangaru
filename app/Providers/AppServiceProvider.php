<?php

namespace App\Providers;
use App\ProductCategory;
use App\NewsCategory;
use App\Product;
use Illuminate\Support\Facades;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if(\Schema::hasTable('product_category') && \Schema::hasTable('news_category')){
            $productCategory = ProductCategory::getAllProductCategory();
            $products = Product::getAllProduct();
            $newsCategory = NewsCategory::getAllNewsCategory();
            view()->share(['productCategory'=>$productCategory, 'newsCategory'=>$newsCategory, 'products'=>$products]);
        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
