<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NewsCategory extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'news_category';

    // use soft delete
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'parrent_id', 'news_count'];

    public static $ruleAdd = ['name'=>'required|unique:news_category,name'];

    public static function getAllNewsCategory()
    {
        return NewsCategory::paginate(PER_PAGE);
    }

    public static function addAndEditCategory($input)
    {
        if(isset($input['add'])) {
            $newsCategory['name'] = $input['name'];
            NewsCategory::create($newsCategory);
        } elseif(isset($input['edit'])) {
            $newsCategory['name'] = $input['edit_name_' . $input['edit']];
            NewsCategory::find($input['edit'])->update($newsCategory);
        }
    }

    public static function deleteNewsCategory($id)
    {
        NewsCategory::find($id)->delete();
    }
}
