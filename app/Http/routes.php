<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/manage/login', ['as'=>'get.login', 'uses'=>'UserController@getLogin']);

Route::post('/manage/login', ['as'=>'post.login', 'uses'=>'UserController@postLogin']);

Route::get('/manage/logout', ['as'=>'get.logout', 'uses'=>'UserController@getLogout']);

Route::group(['middleware' => 'auth', 'prefix'=>'/manage'], function () {

    Route::get('/', ['as'=>'get.top.admin', 'uses'=>'AdminController@getTopAdmin']);

    // Product Category
    Route::get('/product_category/add', ['as'=>'get.add.prd_cate', 'uses'=>'AdminController@getAddProductCategory']);

    Route::post('/product_category/add', ['as'=>'post.add.prd_cate', 'uses'=>'AdminController@postAddProductCategory']);

    Route::get('/product_category/delete/{id}', ['as'=>'get.delete.prd_cate', 'uses'=>'AdminController@getDeleteProductCategory']);

    Route::get('/product_category/detail/{id}', ['as'=>'get.detail.prd_cate', 'uses'=>'AdminController@getDetailProductCategory']);

    // News category
    Route::get('/news_category/add', ['as'=>'get.add.news_cate', 'uses'=>'AdminController@getAddNewsCategory']);

    Route::post('/news_category/add', ['as'=>'post.add.news_cate', 'uses'=>'AdminController@postAddNewsCategory']);

    Route::get('/news_category/detail/{id}', ['as'=>'get.detail.news_cate', 'uses'=>'AdminController@getDetailNewsCategory']);

    Route::get('/news_category/delete/{id}', ['as'=>'get.delete.news_cate', 'uses'=>'AdminController@getDeleteNewsCategory']);

    // Product
    Route::get('/product_category/{id}/addproduct', ['as'=>'get.add.product', 'uses'=>'AdminController@getAddProduct']);

    Route::post('/product_category/{id}/addproduct', ['as'=>'post.add.product', 'uses'=>'AdminController@postAddProduct']);

    Route::get('/editproduct/{id}', ['as'=>'get.edit.product', 'uses'=>'AdminController@getEditProduct']);

    Route::post('/editproduct/{id}', ['as'=>'post.edit.product', 'uses'=>'AdminController@postEditProduct']);

    Route::get('/deleteproduct/{id}', ['as'=>'get.delete.product', 'uses'=>'AdminController@getDeleteProduct']);

    // News
    Route::get('/news_category/{id}/addnews', ['as'=>'get.add.news', 'uses'=>'AdminController@getAddNews']);

    Route::post('/news_category/{id}/addnews', ['as'=>'post.add.news', 'uses'=>'AdminController@postAddNews']);

    Route::get('/editnews/{id}', ['as'=>'get.edit.news', 'uses'=>'AdminController@getEditNews']);

    Route::post('/editnews/{id}', ['as'=>'post.edit.news', 'uses'=>'AdminController@postEditNews']);

    Route::get('/deletenews/{id}', ['as'=>'get.delete.news', 'uses'=>'AdminController@getDeleteNews']);

    //Order
    Route::get('/order', ['as'=>'get.order', 'uses'=>'AdminController@getOrder']);

});

Route::group(['prefix'=>'/'], function () {

    Route::get('/', ['as'=>'get.home', 'uses'=>'FrontController@getHome']);

    Route::get('/product_detail/{id}', ['as'=>'get.product.detail', 'uses'=>'FrontController@getDetailProduct']);

    Route::get('/news/{id}', ['as'=>'get.news', 'uses'=>'FrontController@getNews']);

    Route::get('/buy/{id}', ['as'=>'get.buy', 'uses'=>'FrontController@getBuy']);

    Route::post('/buy/{id}', ['as'=>'post.buy', 'uses'=>'FrontController@postBuy']);

    Route::get('/contact', ['as'=>'get.contact', 'uses'=>'FrontController@getContact']);

    Route::get('/category/{id}', ['as'=>'get.productList', 'uses'=>'FrontController@getProductList']);
});