<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    function getLogin()
    {

        return view('admin.getlogin');
    }

    function postLogin()
    {
        $input = \Input::all();
        $rule = User::$ruleLogin;
        $validation = \Validator::make($input, $rule);
        $errors = $validation->messages();
        if($validation->fails()){

            return \Redirect::route('get.login')->with('errors', $errors);
        }
        if (!\Auth::attempt(array('account' => $input['account'], 'password' => $input['password'])))
        {

            return \Redirect::route('get.login')->with('messages', 'Tài khoản hoặc mật khẩu không đúng');
        }

        return \Redirect::route('get.top.admin');
    }

    function getLogout()
    {
        \Auth::logout();

        return redirect()->route('get.login');
    }
}
