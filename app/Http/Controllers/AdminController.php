<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\News;
use App\ProductCategory;
use App\NewsCategory;
use App\Order;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{

    public function getTopAdmin()
    {
        $products = Product::getAllProduct();
        $news = News::getAllNews();

        return view('admin.top')->with(['products'=>$products, 'news'=>$news]);
    }

    public function getAddProductCategory()
    {
        return view('admin.product_category.addProductCategory');
    }

    public function postAddProductCategory()
    {
        $input = \Input::all();
        if(isset($input['add'])) {
            $rule = ProductCategory::$ruleAdd;
        } elseif(isset($input['edit'])) {
            $rule = ['edit_name_'.$input['edit']=>'required|unique:product_category,name'];
        }
        $validation = \Validator::make($input, $rule);
        $errors = $validation->messages();
        if($validation->fails()){

            return \Redirect::route('get.add.prd_cate')->with('errors', $errors);
        }
        ProductCategory::addAndEditCategory($input);

        return redirect()->route('get.add.prd_cate');
    }

    public function getDetailProductCategory($id)
    {
        $products = Product::getProductByCategory($id);

        return view('admin.product_category.detailProductCategory')->with(['products'=>$products, 'id'=>$id]);
    }

    public function getDeleteProductCategory($id)
    {
        ProductCategory::deleteProductCategory($id);

        return redirect()->route('get.add.prd_cate');
    }

    public function getAddNewsCategory()
    {
        return view('admin.news_category.addNewsCategory');
    }

    public function postAddNewsCategory()
    {
        $input = \Input::all();
        if(isset($input['add'])) {
            $rule = NewsCategory::$ruleAdd;
        } elseif(isset($input['edit'])) {
            $rule = ['edit_name_'.$input['edit']=>'required|unique:product_category,name'];
        }
        $validation = \Validator::make($input, $rule);
        $errors = $validation->messages();
        if($validation->fails()){

            return \Redirect::route('get.add.news_cate')->with('errors', $errors);
        }
        NewsCategory::addAndEditCategory($input);

        return redirect()->route('get.add.news_cate');
    }

    public function getDeleteNewsCategory($id)
    {
        NewsCategory::deleteNewsCategory($id);

        return redirect()->route('get.add.news_cate');
    }

    public function getDetailNewsCategory($id)
    {
        $news = News::getNewsByCategory($id);

        return view('admin.news_category.detailNewsCategory')->with(['news'=>$news, 'id'=>$id]);
    }

    public function getAddProduct($id)
    {
        return view('admin.product.addProduct')->with(['id'=>$id]);
    }

    public function postAddProduct($id)
    {
        $input = \Input::all();
        $rule = Product::$ruleAdd;
        $validation = \Validator::make($input, $rule);
        $errors = $validation->messages();
        if($validation->fails()){

            return \Redirect::route('get.add.product', $id)->with(['id'=>$id, 'errors'=>$errors]);
        }
        Product::addProduct($input, $id);

        return redirect()->route('get.detail.prd_cate', $id);
    }

    public function getDeleteProduct($id)
    {
        $category_id = Product::find($id)->product_category_id;
        Product::find($id)->delete();

        return redirect()->route('get.detail.prd_cate', $category_id);
    }

    public function getEditProduct($id)
    {
        $product = Product::find($id);

        return view('admin.product.editProduct')->with('product', $product);
    }

    public function postEditProduct($id)
    {
        $input = \Input::all();

        $category_id = Product::find($id)->product_category_id;
        // $rule = Product::$ruleAdd;
        $rule = [
                'name' => 'required|unique:products,name,' . $id,
                'sign' => 'required|unique:products,sign,' . $id
                ];
        $validation = \Validator::make($input, $rule);
        $errors = $validation->messages();
        if($validation->fails()){

            return \Redirect::route('get.edit.product', $id)->with('errors', $errors);
        }

        Product::editProduct($input, $id);

        return redirect()->route('get.detail.prd_cate', $category_id);
    }

    public function getAddNews($id)
    {
        return view('admin.news.addNews')->with(['id'=>$id]);
    }

    public function postAddNews($id)
    {
        $file = \Input::file('path_img');
        $name = $file->getClientOriginalName();
        $file->move(public_path().'/images/news', $name);

        $input = \Input::all();
        $rule = [
                'name' => 'required|unique:news,name,',
                'detail' => 'required|unique:news,detail,'
                ];
        $validation = \Validator::make($input, $rule);
        $errors = $validation->messages();
        if($validation->fails()){

            return \Redirect::route('get.add.news', $id)->with('errors', $errors);
        }

        News::addNews($input, $id);

        return redirect()->route('get.detail.news_cate', $id);
    }

    public function getDeleteNews($id)
    {

        $category_id = News::find($id)->news_category_id;
        News::find($id)->delete();
        return redirect()->route('get.detail.news_cate', $category_id);
    }

    public function getEditNews($id)
    {
        $news = News::find($id);
        return view('admin.news.editNews')->with(['id'=>$id, 'news'=>$news]);
    }

    public function postEditNews($id)
    {
        $file = \Input::file('path_img');
        $name = $file->getClientOriginalName();
        $file->move(public_path().'/images/news', $name);
        
        $input = \Input::all();
        $category_id = News::find($id)->news_category_id;
        $rule = [
                'name' => 'required|unique:news,name,'. $id,
                'detail' => 'required|unique:news,detail,'. $id,
                ];
        $validation = \Validator::make($input, $rule);
        $errors = $validation->messages();
        if($validation->fails()){

            return \Redirect::route('get.edit.news', $id)->with('errors', $errors);
        }

        News::editNews($input, $id);

        return redirect()->route('get.detail.news_cate', $category_id);
    }

    public function getOrder()
    {
        $orders = Order::showOrderAdmin();

        return view('admin.order')->with(['orders'=>$orders]);
    }
}
