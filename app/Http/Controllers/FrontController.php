<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\News;
use App\Image;
use App\Order;
use App\ProductCategory;
use App\NewsCategory;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class FrontController extends Controller
{
    public function getHome()
    {
        $products = Product::all();
        $categorys = ProductCategory::all();
        $news = News::getAllNews()->toArray();

        return view('front.home')->with(['products'=>$products, 'news'=>$news, 'categorys'=>$categorys]);
    }

    public function getDetailProduct($id)
    {
        $product = Product::find($id);
        $product_img = Image::getProductImage($id)->toArray();
        // dd($product_img);

        return view('front.productDetail')->with(['product'=>$product, 'product_img'=>$product_img]);
    }

    public function getNews($id)
    {
        $news = News::find($id);
        $listnews = News::listNews($id);

        return view('front.news')->with(['news'=>$news, 'listnews'=>$listnews]);
    }

    public function getBuy($id)
    {
        return view('front.buy')->with(['id'=>$id]);
    }

    public function postBuy($id)
    {
        $input = \Input::all();
        $rule = Order::$rule;
        $message = Order::$message;
        $validation = \Validator::make($input, $rule, $message);
        $errors = $validation->messages();
        if($validation->fails()) {
            return redirect()->route('get.buy', $id)->with(['errors'=>$errors]);
        }
        Order::insertOrder($input);

        return redirect()->route('get.home');
    }

    public function getContact()
    {
        return view('front.contact');
    }

    public function getProductList($id)
    {
        $products = Product::getProductByCategory($id);
        $category = ProductCategory::find($id);

        return view('front.productList')->with(['products'=>$products, 'category'=>$category]);
    }
}
