<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductCategory extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'product_category';

    // use soft delete
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'parrent_id', 'product_count'];

    public static $ruleAdd = ['name'=>'required|unique:product_category,name'];

    public static $ruleEdit = ['edit_name'=>'required|unique:product_category,name'];

    public static function addAndEditCategory($input)
    {
        if(isset($input['add'])){
            $productCategory['name'] = $input['name'];
            ProductCategory::create($productCategory);
        } elseif(isset($input['edit'])) {
            $productCategory['name'] = $input['edit_name_' . $input['edit']];
            ProductCategory::find($input['edit'])->update($productCategory);
        }
    }

    public static function getAllProductCategory()
    {
        return ProductCategory::paginate(PER_PAGE);
    }

    public static function deleteProductCategory($id)
    {
        ProductCategory::find($id)->delete();
    }
}
