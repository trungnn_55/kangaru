<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'orders';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'tel', 'address', 'email'];

    public static $rule = [
                            'name'=>'required',
                            'email'=>'email',
                            'address'=>'required',
                            'tel'=>'required|numeric'
                        ];

    public static $message = [
                                'name.required' => 'Bạn chưa nhập họ tên',
                                'email.email' => 'Định dạng email không đúng',
                                'address.required' => 'Bạn chưa nhập địa chỉ',
                                'tel.required' => 'Bạn chưa nhập số điện thoại',
                                'tel.numeric' => 'Số điện thoại không đúng định dạng'
                            ];

    public static function insertOrder($input)
    {
        Order::create($input);
    }

    public static function showOrderAdmin()
    {
        $orders = Order::orderBy('created_at', 'desc')->paginate(10);

        return $orders;
    }
}
