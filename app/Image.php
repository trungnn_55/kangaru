<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'images';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['product_id', 'path_img'];

    public static function getProductImage($id)
    {
        $product_image = Image::where('product_id', $id)->get();

        return $product_image;
    }
}
