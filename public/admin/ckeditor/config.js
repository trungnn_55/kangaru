/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
    // config.skin='office2003';
	config.language = 'vi';
    config.removePlugins = 'help';
	config.uiColor = '#AADC6E';

    config.filebrowserBrowseUrl = '/admin/ckfinder/ckfinder.html';

    config.filebrowserImageBrowseUrl = '/admin/ckfinder/ckfinder.html?type=Images';

    config.filebrowserFlashBrowseUrl = '/admin/ckfinder/ckfinder.html?type=Flash';

    config.filebrowserUploadUrl = '/admin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files';

    config.filebrowserImageUploadUrl = '/admin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images';

    config.filebrowserFlashUploadUrl = '/admin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash';
};
