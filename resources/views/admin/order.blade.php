@extends('admin.layout')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Đơn đặt hàng</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class="row">
        <div class="col-lg-8">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Thông tin chi tiết
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            @if($orders->count() >= 1)
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Tên khách hàng</th>
                                    <th>Địa chỉ</th>
                                    <th>Số điện thoại</th>
                                    <th>Email</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($orders as $order)
                                <tr>
                                    <td>{{{ $order->id }}}</td>
                                    <td>{{{ $order->name }}}</td>
                                    <td>{{{ $order->address }}}</td>
                                    <td>{{{ $order->tel }}}</td>
                                    <td>{{{ $order->email }}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                            @else
                            <p class="help-block">Chưa có sản phẩm nào!</p>
                            @endif
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
    </div>
@stop