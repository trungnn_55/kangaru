@extends('admin.layout')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Sản phẩm</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class="row">
        <div class="col-lg-8">
            {!! Form::open(['route'=>['post.add.product', $id], 'files'=>true]) !!}
            <div class="panel panel-default">
                <div class="panel-heading">
                    Thông tin sản phẩm
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        @include('admin.validate.error_validate')
                        <table class="table table-bordered table-hover">
                                <tr>
                                    <th class="well">Tên sản phẩm</th>
                                    <th>{!! Form::text('name', '', ['class'=>'form-control']) !!}</th>
                                </tr>
                                <tr>
                                    <th class="well">Mã sản phẩm</th>
                                    <th>{!! Form::text('sign', '', ['class'=>'form-control']) !!}</th>
                                </tr>
                                <tr>
                                    <th class="well">Ảnh sản phẩm</th>
                                    <th>{!! Form::file('path_img', '', ['class'=>'form-control']) !!}</th>
                                </tr>
                                <tr>
                                    <th class="well">Thông tin chi tiết</th>
                                    <th>{!! Form::textarea('detail', '', ['class'=>'form-control']) !!}</th>
                                </tr>
                                <tr>
                                    <th class="well">Thông tin hiển thị</th>
                                    <th>{!! Form::textarea('show_detail', '', ['class'=>'form-control']) !!}</th>
                                </tr>
                                <tr>
                                    <th class="well">Tính năng sản phẩm</th>
                                    <th>{!! Form::textarea('feature', '', ['class'=>'form-control']) !!}</th>
                                </tr>
                                <tr>
                                    <th class="well">Thông số kỹ thuật</th>
                                    <th>{!! Form::textarea('technical', '', ['class'=>'form-control','id'=>'CKEditor']) !!}</th>
                                </tr>
                        </table>
                        <button type="submit" name="edit" class="btn btn-primary btn-lg">Thêm mới</button>
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
            {!! Form::close() !!}
        </div>
    </div>
    @include('admin.add_ckeditor')
@stop
