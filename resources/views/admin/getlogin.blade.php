<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin 2 - Bootstrap Admin Theme</title>

    <!-- Bootstrap Core CSS -->
    {!! HTML::style('admin/bower_components/bootstrap/dist/css/bootstrap.min.css') !!}

    <!-- MetisMenu CSS -->
    {!! HTML::style('admin/bower_components/metisMenu/dist/metisMenu.min.css') !!}

    <!-- Timeline CSS -->
    {!! HTML::style('admin/dist/css/timeline.css') !!}

    <!-- Custom CSS -->
    {!! HTML::style('admin/dist/css/sb-admin-2.css') !!}

    <!-- Morris Charts CSS -->
    {!! HTML::style('admin/bower_components/morrisjs/morris.css') !!}

    <!-- Custom Fonts -->
    {!! HTML::style('admin/bower_components/font-awesome/css/font-awesome.min.css') !!}

</head>

<body>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Đăng nhập</h3>
                    </div>
                    <div class="panel-body">
                    @if(Session::has('messages'))
                    <div class="alert alert-danger">
                        {!! Session::get('messages') !!}
                    </div>
                    @endif
                    @include('admin.validate.error_validate')
                        {!! Form::open(['route'=>'post.login']) !!}
                            <fieldset>
                                <div class="form-group">
                                    {!! Form::text('account', '', ['class'=>'form-control', 'placeholder'=>'Tài khoản', 'autofocus']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::password('password', ['class'=>'form-control', 'placeholder'=>'Mật khẩu']) !!}
                                </div>
                                <div class="checkbox">
                                    <label>
                                        {{-- {!! Form::checkbox('remember', '1') !!}Ghi nhớ đăng nhập --}}
                                    </label>
                                </div>
                                <!-- Change this to a button or input when using this as a form -->
                                {!! Form::submit('Đăng nhập', ['class'=>'btn btn-lg btn-success btn-block']) !!}
                            </fieldset>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

</body>

</html>
