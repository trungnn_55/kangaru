@extends('admin.layout')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Sản phẩm</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <p>
        <a href="{!! route('get.add.product', $id) !!}" class="btn btn-primary btn-lg">Thêm sản phẩm</a>
    </p><br/>
    <div class="row">
        <div class="col-lg-8">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Danh sách các sản phẩm
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            @if($products->count() >= 1)
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Tên sản phẩm</th>
                                    <th>Thay đổi</th>
                                    <th>Xóa</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($products as $product)
                                <tr>
                                    <td>{{{ $product->id }}}</td>
                                    <td><a href="{!! route('get.detail.prd_cate', $product->id) !!}">{{{ $product->name }}}</a></td>
                                    <td><a href="{!! route('get.edit.product', $product->id) !!}" class="btn btn-primary">Thay đôi</a></td>
                                    <td><a href="{!! route('get.delete.product', $product->id) !!}" class="btn btn-danger">Xoá</a></td>
                                </tr>
                                @endforeach
                            </tbody>
                            @else
                            <p class="help-block">Chưa có sản phẩm nào!</p>
                            @endif
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
    </div>
@stop