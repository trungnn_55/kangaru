@extends('admin.layout')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Tin tức</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class="row">
        <div class="col-lg-8">
            {!! Form::open(['post.add.news_cate']) !!}
            <div class="panel panel-default">
                <div class="panel-heading">
                    Phân loại tin tức
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-6">
                                @include('admin.validate.error_validate')
                                <div class="form-group">
                                    <label>Nhóm tin tức</label>
                                    {!! Form::text('name', '', ['class'=>'form-control']) !!}
                                </div>
                                <button type="submit" name="add" class="btn btn-default">Thêm mới</button>
                        </div>
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Tên</th>
                                    <th>Thay đổi</th>
                                    <th>Xóa</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($newsCategory as $newsCate)
                                <tr>
                                    <td>{{{ $newsCate->id }}}</td>
                                    {{-- <td><a href="{!! route('get.detail.news_cate', $newsCate->id) !!}">{{{ $newsCate->name }}}</a></td> --}}
                                    <td>{!! Form::text('edit_name_'. $newsCate->id, $newsCate->name, ['class'=>'form-control']) !!}</td>
                                    <td>
                                        <button type="submit" name="edit" class="btn btn-primary" value="{{{ $newsCate->id }}}">Thay đổi</button>
                                    </td>
                                    <td><a href="{!! route('get.delete.news_cate', $newsCate->id) !!} " onclick="javascript:return confirm('Bạn có chắc chắn muốn xoá?')" class="btn btn-danger">Xoá</a></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
            {!! Form::close() !!}
        </div>
    </div>

@stop