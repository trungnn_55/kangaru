@extends('admin.layout')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Tin tức</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <p>
        <a href="{!! route('get.add.news', $id) !!}" class="btn btn-primary btn-lg" role="button">Thêm tin tức</a>
    </p><br/>
    <div class="row">
        <div class="col-lg-8">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Danh sách các tin tức
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            @if($news->count() >= 1)
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Tên sản phẩm</th>
                                    <th>Thay đổi</th>
                                    <th>Xóa</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($news as $newsl)
                                <tr>
                                    <td>{{{ $newsl->id }}}</td>
                                    <td><a href="{!! route('get.detail.news_cate', $newsl->id) !!}">{{{ $newsl->name }}}</a></td>
                                    <td><a href="{!! route('get.edit.news', $newsl->id) !!}">Sửa</a></td>
                                    <td><a href="{!! route('get.delete.news', $newsl->id) !!}">Xoá</a></td>
                                </tr>
                                @endforeach
                            </tbody>
                            @else
                            <p class="help-block">Chưa có tin tức nào!</p>
                            @endif
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
    </div>
@stop