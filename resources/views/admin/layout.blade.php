
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Kangaroo</title>

    <!-- Bootstrap Core CSS -->
    {!! HTML::style('admin/bower_components/bootstrap/dist/css/bootstrap.min.css') !!}

    <!-- MetisMenu CSS -->
    {!! HTML::style('admin/bower_components/metisMenu/dist/metisMenu.min.css') !!}

    <!-- Timeline CSS -->
    {!! HTML::style('admin/dist/css/timeline.css') !!}

    <!-- Custom CSS -->
    {!! HTML::style('admin/dist/css/sb-admin-2.css') !!}

    <!-- Morris Charts CSS -->
    {!! HTML::style('admin/bower_components/morrisjs/morris.css') !!}

    <!-- Custom Fonts -->
    {!! HTML::style('admin/bower_components/font-awesome/css/font-awesome.min.css') !!}

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                {!! HTML::linkRoute('get.top.admin', 'Màn hình quản lý', NULL, ['class'=>'navbar-brand']) !!}
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="{{ route('get.logout') }}"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                            <!-- /input-group -->
                        </li>
                        <li>
                            <a href="{{ route('get.top.admin') }}" class="active"><i class="fa fa-dashboard fa-fw"></i> Trang chủ</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-sitemap fa-fw"></i>Loại sản phẩm<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{ route('get.add.prd_cate') }}">Thêm category</a>
                                </li>
                            @if($productCategory->count() >= 1)
                                @foreach($productCategory as $prdCategory)
                                <li>
                                    <a href="{!! route('get.detail.prd_cate', $prdCategory->id) !!}">{{{ $prdCategory->name }}}</a>
                                </li>
                                @endforeach
                            @endif
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-sitemap fa-fw"></i>Tin tức<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{ route('get.add.news_cate') }}">Thêm category</a>
                                </li>
                            @if($newsCategory->count() >= 1)
                                @foreach($newsCategory as $newsCate)
                                <li>
                                    <a href="{!! route('get.detail.news_cate', $newsCate->id) !!}">{{{ $newsCate->name }}}</a>
                                </li>
                                @endforeach
                            @endif
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="{!! route('get.order') !!}"><i class="fa fa-sitemap fa-fw"></i>Đơn hàng<span class="fa arrow"></span></a>
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>
        <div id="page-wrapper" style="min-height: 549px;">
            @yield('content')
            <!-- /.container-fluid -->
        </div>
    </div>

    {!!Html::script('admin/bower_components/jquery/dist/jquery.min.js')!!}

    <!-- Bootstrap Core JavaScript -->
    {!!Html::script('admin/bower_components/bootstrap/dist/js/bootstrap.min.js')!!}
    <!-- Metis Menu Plugin JavaScript -->
    {!!Html::script('admin/bower_components/metisMenu/dist/metisMenu.min.js')!!}
    <!-- Custom Theme JavaScript -->
    {!!Html::script('admin/dist/js/sb-admin-2.js')!!}
    <script>
    // tooltip demo
    $('.tooltip-demo').tooltip({
        selector: "[data-toggle=tooltip]",
        container: "body"
    })

    // popover demo
    $("[data-toggle=popover]")
        .popover()
    </script>
</body>