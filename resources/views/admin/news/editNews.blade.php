@extends('admin.layout')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Tin tức</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class="row">
        <div class="col-lg-8">
            {!! Form::open(['route'=>['post.edit.news', $id], 'files'=>true]) !!}
            <div class="panel panel-default">
                <div class="panel-heading">
                    Biên tập bài viết
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        @include('admin.validate.error_validate')
                        <table class="table table-bordered table-hover">
                                <tr>
                                    <th class="well">Tiêu đề</th>
                                    <th>{!! Form::text('name', nl2br($news->name), ['class'=>'form-control']) !!}</th>
                                </tr>
                                <tr>
                                    <th class="well">Ảnh tiêu đề</th>
                                    <th>
                                        {!! Form::file('path_img', '', ['class'=>'form-control']) !!}
                                    </th>
                                </tr>
                                <tr>
                                    <th class="well">Biên tập bài viết</th>
                                    <th>{!! Form::textarea('detail', nl2br($news->detail), ['class'=>'form-control','id'=>'CKEditor']) !!}</th>
                                </tr>
                        </table>
                        <button type="submit" name="edit" class="btn btn-primary btn-lg">Thay đổi</button>
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
            {!! Form::close() !!}
        </div>
    </div>
    {{-- <script src="ckeditor/ckeditor.js"></script> --}}
    <script src="http://code.jquery.com/jquery-2.1.4.min.js"></script>
    <script src="/admin/ckeditor/ckeditor.js"></script>

    <script>
      $(function () {
            CKEDITOR.replace('CKEditor');
      });
    </script>
@stop
