<div class="col-sm-3">
    <div class="left-sidebar">
        <h2>Dòng sản phẩm</h2>
        <div class="panel-group category-products" id="accordian"><!--category-productsr-->
        @foreach($productCategory as $prdCategory)
            @if(getProductByProductCategory($prdCategory->id)->count() < 0)
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordian" href="#sportswear">
                            <span class="badge pull-right"><i class="fa fa-plus"></i></span>
                            {{{ $prdCategory->name }}}
                        </a>
                    </h4>
                </div>
                <div id="sportswear" class="panel-collapse collapse">
                    <div class="panel-body">
                        <ul>
                        @foreach(getProductByProductCategory($prdCategory->id) as $productCate)

                            <li><a href="{!! route('get.product.detail', $productCate->id) !!}">{{{ $productCate->name }}}</a></li>
                        @endforeach
                        </ul>
                    </div>
                </div>
            </div>
            @else
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title"><a href="{!! route('get.productList', $prdCategory->id) !!}">{{{ $prdCategory->name }}}</a></h4>
                </div>
            </div>
            @endif
        @endforeach
        </div><!--/category-products-->

       

        <div class="shipping text-center"><!--shipping-->
            <img src="/front/images/home/shipping.jpg" alt="" />
        </div><!--/shipping-->

    </div>
</div>