@extends('front.main')

@section('title') Thông tin liên hệ @stop

@section('content')

<section>
    <div class="container">
        <div class="row">
            @include('front.common.left-sidebar')
            <div class="col-sm-9">
            <div class="step-one">
                <h2 class="heading">Thông tin liên hệ</h2>
                <p> Đinh Việt Hùng<br/>
				Tel: 01299012688<br/>
				Địa chỉ 521 Nguyễn Trãi Quận Thanh Xuân Thành Phố Hà Nội
				</p>
            </div>
            </div>
        </div>
    </div>
@stop