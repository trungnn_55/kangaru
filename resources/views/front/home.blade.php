@extends('front.main')

@section('title') KANGAROO365-SHOP @stop

@section('content')
    <section id="slider"><!--slider-->
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div id="slider-carousel" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#slider-carousel" data-slide-to="1"></li>
                            <li data-target="#slider-carousel" data-slide-to="2"></li>
                            <li data-target="#slider-carousel" data-slide-to="3"></li>
                        </ol>

                        <div class="carousel-inner">
                        @foreach($products as $product)
                            @if($product->id == $products->first()->id)
                            <div class="item active">
                            @else
                            <div class="item ">
                            @endif
                                <div class="col-sm-6">
                                    <h1><span>KANGAROO</span>-SHOP</h1>
                                    <h2>{{{ $product->name }}}</h2>
                                    <p>{!! nl2br($product->show_detail) !!}</p>
                                    <a href="{!! route('get.buy', $product->id) !!}" type="button" class="btn btn-default get">Mua ngay</a>
                                </div>
                                <div class="col-sm-6">
                                    <img src="{{{ $product->path_img }}}" class="girl img-responsive" alt="" />
                                    <!-- <img src="front/images/home/pricing.png"  class="pricing" alt="" /> -->
                                </div>
                            </div>
                        @endforeach
                        </div>

                        <a href="#slider-carousel" class="left control-carousel hidden-xs" data-slide="prev">
                            <i class="fa fa-angle-left"></i>
                        </a>
                        <a href="#slider-carousel" class="right control-carousel hidden-xs" data-slide="next">
                            <i class="fa fa-angle-right"></i>
                        </a>
                    </div>

                </div>
            </div>
        </div>
    </section><!--/slider-->

    <section>
        <div class="container">
            <div class="row">
                @include('front.common.left-sidebar')
                <div class="col-sm-9 padding-right">
                    @foreach($categorys as $category)
                    <div class="features_items"><!--features_items-->
                        <h2 class="title text-center">{!! $category->name !!}</h2>
                        @foreach(getProductByProductCategory($category->id, 3) as $product)
                        <div class="col-sm-4">
                            <div class="product-image-wrapper">
                                <div class="single-products" weight: 250px height:350px>
                                    <div class="productinfo text-center">
                                        <a href="{{ route('get.product.detail', $product->id) }}"><img src="{{{ $product->path_img }}}" alt="" /></a></br>
                                        <h5>{!! $product->name !!}</h5>
                                        <p>{!! nl2br($product->show_detail) !!}</p>
                                        <a href="{!! route('get.buy', $product->id) !!}" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Đặt mua</a>
                                    </div>
                                </div>  
                            </div>
                        </div>
                        @endforeach
                    </div><!--features_items-->
                    @endforeach

                    <div class="recommended_items"><!--recommended_items-->
                        <h2 class="title text-center">Tin tức</h2>

                        <div id="recommended-item-carousel" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner">
                            @for($i = 0; $i < count($news)/3; $i++ )
                                @if($i == 0)
                                <div class="item active">
                                @else
                                <div class="item">
                                @endif
                                @for($j = $i*3; $j < $i*3+3 && $j<count($news); $j++)
                                    <div class="col-sm-4">
                                        <div class="product-image-wrapper">
                                            <div class="single-products">
                                                <div class="productinfo text-center">
                                                    <a href="{{ route('get.news', $news[$j]['id']) }}">
                                                    <img src="{{{ $news[$j]['path_img'] }}}" alt="" />
                                                    <p>{{{ $news[$j]['name'] }}}</p>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endfor
                                </div>
                            @endfor
                            </div>
                             <a class="left recommended-item-control" href="#recommended-item-carousel" data-slide="prev">
                                <i class="fa fa-angle-left"></i>
                              </a>
                              <a class="right recommended-item-control" href="#recommended-item-carousel" data-slide="next">
                                <i class="fa fa-angle-right"></i>
                              </a>
                        </div>
                    </div><!--/recommended_items-->
                    
                </div>
            </div>
        </div>
    </section>

@stop