@extends('front.main')

@section('content')

<section id="cart_items">
        <div class="container">
            <div class="breadcrumbs">
                <ol class="breadcrumb">
                  <li><a href="{!! route('get.home') !!}">Home</a></li>
                  <li class="active">Đăng ký thông tin</li>
                </ol>
            </div><!--/breadcrums-->

            <div class="step-one">
                <h2 class="heading">Đặt mua sản phẩm</h2>
            </div>
            <!--/checkout-options-->

            <!--/register-req-->

            <div class="shopper-informations">
                <div class="row">
                    <div class="col-sm-8">
                        <div class="shopper-info">
                            <p>Nhập thông tin cá nhân</p>
                            @if($errors->has())
                            <div class="alert alert-danger">
                                @foreach($errors->all() as $error)
                                {!! $error !!}</br>
                                @endforeach
                            </div>
                            @endif
                            {!! Form::open(['route'=>['post.buy', $id]]) !!}
                                {!! Form::text('name', '', ['placeholder'=>'Họ Tên']) !!}
                                {!! Form::text('address', '', ['placeholder'=>'Địa chỉ']) !!}
                                {!! Form::text('tel', '', ['placeholder'=>'Số điện thoại']) !!}
                                {!! Form::text('email', '', ['placeholder'=>'Email']) !!}
                                {!! Form::submit('Xác nhận', ['class'=>'btn btn-primary']) !!}
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@stop