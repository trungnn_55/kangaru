@extends('front.main')

@section('title') {!! $product->name !!} @stop

@section('content')
    <section>
        <div class="container">
            <div class="row">
                @include('front.common.left-sidebar')
                <div class="col-sm-9 padding-right">
                    <div class="product-details"><!--product-details-->
                        <div class="col-sm-5">
                            <div class="view-product">
                                <img src="{{ $product->path_img }}" alt="" />
                                <h3>ZOOM</h3>
                            </div>
                            <div id="similar-product" class="carousel slide" data-ride="carousel">
                                  <!-- Wrapper for slides -->
                                    <div class="carousel-inner">
                                    @for($i = 0; $i < count($product_img)/3; $i++)
                                        @if($i == 0)
                                            <div class="item active">
                                        @else
                                            <div class="item">
                                        @endif

                                        @for($j = $i*3; $j < $i*3+3 && $j<count($product_img); $j++)
                                          <a href=""><img src="{!! $product_img[$j]['path_img'] !!}" alt="" width="84px"></a>
                                        @endfor
                                        </div>
                                    @endfor
                                    </div>

                                  <!-- Controls -->
                                  <a class="left item-control" href="#similar-product" data-slide="prev">
                                    <i class="fa fa-angle-left"></i>
                                  </a>
                                  <a class="right item-control" href="#similar-product" data-slide="next">
                                    <i class="fa fa-angle-right"></i>
                                  </a>
                            </div>

                        </div>
                        <div class="col-sm-7">
                            <div class="product-information"><!--/product-information-->
                                <h2>{!! $product->name !!}</h2>
                                <p>Mã sản phẩm: {!! $product->sign !!}</p>
                                <p>{!! nl2br($product->detail) !!}</p>
                                <button type="button" class="btn btn-fefault cart">
                                    <i class="fa fa-shopping-cart"></i>
                                    Đặt hàng
                                </button>
                            </div><!--/product-information-->
                        </div>
                    </div><!--/product-details-->

                    <div class="category-tab shop-details-tab"><!--category-tab-->
                        <div class="col-sm-12">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#details" data-toggle="tab">Tính năng sản phẩm</a></li>
                                <li><a href="#companyprofile" data-toggle="tab">Đặc tính kỹ thuật</a></li>
                                <li><a href="#tag" data-toggle="tab">Hướng dẫn sử dụng</a></li>
                            </ul>
                        </div>
                        <div class="tab-content">
                            <div class="tab-pane fade  active in" id="details" >
                                <div class="col-sm-12">
                                    <p>{!! nl2br($product->feature) !!}</p>
                                </div>
                            </div>

                            <div class="tab-pane fade" id="companyprofile" >
                                <div class="col-sm-12">
                                    <p>{!! $product->technical !!}</p>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="tag" >
                            </div>
                            <div class="tab-pane fade" id="reviews" >
                            </div>
                        </div>
                    </div><!--/category-tab-->
                    <div class="recommended_items"><!--recommended_items-->
                        {{-- {!! $product->technical !!} --}}
                    </div><!--/recommended_items-->

                </div>
            </div>
        </div>
    </section>

@stop