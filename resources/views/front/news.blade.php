@extends('front.main')

@section('title') {!! $news->name !!} @stop 

@section('content')

<section>
    <div class="container">
        <div class="row">
            @include('front.common.left-sidebar')
            <div class="col-sm-9">
                    <div class="blog-post-area">
                        <h2 class="title text-center">Tin tức</h2>
                        <div class="single-blog-post">
                            <h3>{{{ $news->name }}}</h3>
                            <div class="post-meta">
                                <ul>
                                    <li><i class="fa fa-clock-o"></i>{{{ date('h:i:s', strtotime($news->created_at)) }}}</li>
                                    <li><i class="fa fa-calendar"></i>{{{ date('Y-M-D', strtotime($news->created_at)) }}}</li>
                                </ul>
                            </div>
                            {!! $news->detail !!}
                        </div>
                    </div><!--/blog-post-area-->

                    <div class="media commnets">
                        <div class="media-body">
                            <h4 class="media-heading">Các bài liên quan</h4>
                            @foreach($listnews as $listnewsl)
                            <li><p><a href="{{ route('get.news', $listnewsl->id) }}">{{{ $listnewsl->name }}}</a></p></li>
                            @endforeach
                        </div>
                    </div>
                </div>
        </div>
    </div>

@stop